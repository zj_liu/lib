/*
 * @Author: LZJ
 * @since: 2020-04-18
 * @lastTime: 2020-04-18
 * @LastAuthor: Do not edit
 * @FilePath: \include\td_list.h
 * @Description: 
 */
#ifndef TD_LIST_H
#define TD_LIST_H
/***********************************************Include***********************************************/
#include <stdio.h>
#include <stdbool.h>

#include "td_type.h"
/***********************************Global Definitions and Declarations*******************************/
/***********************************Constant / Macro Definitions**************************************/
/*******************************************Type Declarations*****************************************/
typedef struct td_node_t
{
	struct td_node_t* prev;
	struct td_node_t* next;
}TD_NODE_T,*TD_NODE_P;

typedef struct td_list_t
{
	/* M E M B E R    V A R I A B L E S */
	TD_NODE_P head;		        /* the first node of the list */
	TD_NODE_P tail;		        /* the last node of list */
	int	n_node;					/* number of nodes */


	/* M E M B E R    F U N C T I O N S */
	/* This method adds a specified node to the end of a specified list. */
	TD_STATUS (*add)(struct td_list_t* const me,TD_NODE_P node);
	/* This method removes a specified node from a specified list. */
	TD_STATUS (*remove)(struct td_list_t* const me,TD_NODE_P node);
	/* This method inserts a specified node in a specified list. 
	The new node is placed following the list node prev_node. 
	If prev_node is NULL, the node is inserted at the head of the list. */
	TD_STATUS (*insert)(struct td_list_t* const me,TD_NODE_P prev_node,TD_NODE_P node);
	/*This method concatenates the second list to the end of the first list. 
	The second list is left empty. Either list (or both) can be empty at the beginning of the operation.*/
	TD_STATUS (*concat)(struct td_list_t* const me,struct td_list_t* const add_list);
	/* This method extracts the sublist that starts with start_node and ends with end_node from list "me".
	It places the extracted list in dst_list. */
	TD_STATUS (*extract)(struct td_list_t* const me,TD_NODE_P start_node,TD_NODE_P end_node,struct td_list_t* const dst_list);
	/* This method turns any list into an empty list. 
	It also frees up memory used for nodes. */
	TD_STATUS (*free)(struct td_list_t* const me);
	/* This method returns the number of nodes in a specified list. */
	int	(*count)(struct td_list_t* const me);
}TD_LIST_T,*TD_LIST_P;
/******************************************Variable Declarations**************************************/
/*******************************************Function Prototype****************************************/
PUBLIC	TD_LIST_P   td_list_new(void);
PUBLIC	TD_STATUS	td_list_delete(TD_LIST_P const me);


#endif  /*td_list.h*/
