/*
 * @Description: 
 * @Date: 2020-04-25 20:08:50
 * @LastEditors: LZJ
 * @LastEditTime: 2020-05-05 16:09:24
 * @FilePath: /include/td_type.h
 */

#ifndef TD_ERR_TYPE_H
#define TD_ERR_TYPE_H



#define PRIVATE static
#define PUBLIC extern


typedef enum td_status_t
{
	TD_OK=0,
	TD_SUCCESS,
	TD_TRUE,
	TD_FALSE,
	TD_FAILURE,
	TD_ERR_UNKNOWN,
	TD_ERR_QUEUE_FULL,
	TD_ERR_QUEUE_EMPTY,
	TD_ERR_LIST_IS_NULL,
	TD_ERR_NODE_NOT_FOUND,
	TD_ERR_INVALID_PARAM,
	TD_ERR_STACK_IS_NULL,
	TD_ERR_STACK_EMPTY,
	TD_ERR_TREE_NODE_IS_EXIT,
	TD_ERR_TREE_IS_EMPTY
}TD_STATUS;
#endif  /*td_err_type.h*/