/*
 * @Description: 
 * @Date: 2020-05-04 13:33:40
 * @LastEditors: LZJ
 * @LastEditTime: 2020-05-04 15:24:12
 * @FilePath: /include/log.h
 */
#ifndef LOG_H
#define LOG_H
/***********************************************Include***********************************************/
/***********************************Global Definitions and Declarations*******************************/
/***********************************Constant / Macro Definitions**************************************/
#define LOG_INFO 0
#define LOG_WARRING 1
#define LOG_DEBUG 2
#define LOG_ERROR 3

// extern int log_lever;

#define LOG_MES(lever,f,...) do{\
if (lever>=LOG_ERROR){ fprintf(stderr,"\033[1;31m LOG_ERROR \033[0m  %d:%s():"f"\n",__LINE__,__FUNCTION__,##__VA_ARGS__);} \
else if(lever>=LOG_DEBUG){ fprintf(stderr,"\033[1;34m LOG_DEBUG \033[0m  %d:%s():"f"\n",__LINE__,__FUNCTION__,##__VA_ARGS__);}\
else if(lever>=LOG_WARRING){ fprintf(stderr,"\033[1;33m LOG_DEBUG \033[0m  %d:%s():"f"\n",__LINE__,__FUNCTION__,##__VA_ARGS__);}\
else if(lever>=LOG_INFO){ fprintf(stderr,"%d:%s():"f"\n",__LINE__,__FUNCTION__,##__VA_ARGS__);}\
else{}\
}while(0)

#define LOG(lever,f,...)  do{\
if (lever<=log_lever)\
{ LOG_MES(lever,f,##__VA_ARGS__);}\
}while (0)
/*******************************************Type Declarations*****************************************/
/******************************************Function Declarations**************************************/
/******************************************Variable Declarations**************************************/
#endif  /*log.h*/