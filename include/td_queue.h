/*
 * @Description: 
 * @Date: 2020-04-26 15:33:09
 * @LastEditors: LZJ
 * @LastEditTime: 2020-04-26 15:42:02
 * @FilePath: \include\td_queue.h
 */
#ifndef TD_QUEUE_H
#define TD_QUEUE_H
/***********************************************Include***********************************************/
#include <stdio.h>
#include <stdbool.h>

#include "td_type.h"
/***********************************Global Definitions and Declarations*******************************/
/***********************************Constant / Macro Definitions**************************************/
/*******************************************Type Declarations*****************************************/
typedef struct td_queue_node
{
    struct td_queue_node * prex;  
    struct td_queue_node * next;    
}TD_QUEUE_NODE_T,*TD_QUEUE_NODE_P;

typedef struct td_queue
{
    TD_QUEUE_NODE_P front; /* 队列头指针 */
    TD_QUEUE_NODE_P rear; /* 队列尾指针 */
    int n_node;
    /* 获取队列头部元素 */
    TD_QUEUE_NODE_P (*get_front)   ( struct td_queue*  const me);
    /* 获取队列尾部元素 */
    TD_QUEUE_NODE_P (*get_rear)   ( struct td_queue*  const me);
    /* 入对 */
    TD_STATUS       (*EnQueue)  ( struct td_queue*  const me,TD_QUEUE_NODE_P node);
    /* 出对 */
    TD_STATUS       (*DeQueue)   ( struct td_queue*  const me);
    /* 清空队列 */
    TD_STATUS       (*free)  ( struct td_queue*  const me);
    /* 获取队列元素数量 */
    int             (*count) ( struct td_queue*  const me);
}TD_QUEUE_T,*TD_QUEUE_P;
/******************************************Variable Declarations**************************************/
/*******************************************Function Prototype****************************************/
PUBLIC TD_QUEUE_P td_queue_new(void);
PUBLIC TD_STATUS  td_queue_delete(TD_QUEUE_P const me);

#endif  /*td_queue.h*/