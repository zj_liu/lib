/*
 * @Description: 
 * @Date: 2020-04-26 15:32:58
 * @LastEditors: LZJ
 * @LastEditTime: 2020-05-12 09:52:41
 * @FilePath: /include/td_stack.h
 */
#ifndef TD_STACK_H
#define TD_STACK_H
/***********************************************Include***********************************************/
#include <stdio.h>
#include <stdbool.h>

#include "td_type.h"
/***********************************Global Definitions and Declarations*******************************/
/***********************************Constant / Macro Definitions**************************************/
/*******************************************Type Declarations*****************************************/
typedef struct td_stack_node
{
    struct td_stack_node * next;    
}TD_STACK_NODE_T,*TD_STACK_NODE_P;

typedef struct td_stack
{
    TD_STACK_NODE_P head; /* 栈顶指针 */
    int n_node;
    /* 获取栈顶元素 */
    TD_STACK_NODE_P (*get)   ( struct td_stack*  const me);
    /* 入栈 */
    TD_STATUS       (*push)  ( struct td_stack*  const me,TD_STACK_NODE_P node);
    /* 出栈 */
    TD_STACK_NODE_P       (*pop)   ( struct td_stack*  const me);
    /* 清空栈 */
    TD_STATUS       (*free)  ( struct td_stack*  const me);
    /* 获取栈元素数量 */
    int             (*count) ( struct td_stack*  const me);
}TD_STACK_T,*TD_STACK_P;
/******************************************Variable Declarations**************************************/
/*******************************************Function Prototype****************************************/
PUBLIC TD_STACK_P td_stack_new(void);
PUBLIC TD_STATUS td_stack_delete(TD_STACK_P const me);
#endif  /*td_stack.h*/