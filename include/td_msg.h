/*
 * @Description: 
 * @Date: 2020-05-04 14:17:27
 * @LastEditors: LZJ
 * @LastEditTime: 2020-05-04 18:48:00
 * @FilePath: /include/td_msg.h
 */

#ifndef TD_MSG_H
#define TD_MSG_H
/***********************************************Include***********************************************/
/***********************************Global Definitions and Declarations*******************************/
#define MAX_MSG_LEN 1024*2
/***********************************Constant / Macro Definitions**************************************/
typedef enum msg_key
{
    test1=10,
    test2,
    test3,
    test4,
}MSG_KEY;
typedef enum msg_flag
{
    aa,
};
/*******************************************Type Declarations*****************************************/
/*因为消息传输大小要求*/
#pragma pack(1)
typedef struct td_msg_body
{
    long type;
    int length;
    char *buf;
    char* 	(*allocate)(struct td_msg_body* me,int length);	//allocate mem for buf
	int   		(*free)(struct td_msg_body* me);//free 
}TD_MSG_T,*TD_MSG_P;
#pragma pack()
typedef struct td_msg_queue_tag
{
	
	//public
	//Creating message queues
	int (*create)( struct td_msg_queue_tag* me);
	//Delete message queue
	int (*destroy)( struct td_msg_queue_tag* me); 
	//Send a message to the message queue
	int (*post)( struct td_msg_queue_tag* me,TD_MSG_P pmsg);
	//Receive messages from the message queue and save them to the PMSG
	int (*pend)( struct td_msg_queue_tag* me,TD_MSG_P pmsg);
		
	//private
	int key;	//message key
	int id ;    //message id
}TD_MSG_Q_T,*TD_MSG_Q_P;
/******************************************Function Declarations**************************************/
/******************************************Variable Declarations**************************************/

#endif  /*td_msg.h*/

