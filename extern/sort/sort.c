/*
 * @Description: 
 * @Date: 2020-05-01 14:33:35
 * @LastEditors: LZJ
 * @LastEditTime: 2020-05-04 10:16:59
 * @FilePath: /extern/sort/sort.c
 */
/***********************************************Include***********************************************/
#include <stdio.h>
/***********************************Global Definitions and Declarations*******************************/
/***********************************Constant / Macro Definitions**************************************/
/*******************************************Type Declarations*****************************************/
/******************************************Variable Declarations**************************************/
/*******************************************Function Prototype****************************************/
void print_int(int *src,int len)
{
    printf("len=%d,d[]=: ",len);
    for(int a=0;a<len;a++)
        printf(" %d",src[a]);
    printf("\n");
}
/* 快速排序以中间元素为比较 
*  stdlib.h中含有qsort快排函数
*   int (*cmp)(const void *,const void *);
*   qsort(*s, n, sizeof(s[0]), cmp);
*
*   qsort(a, 1000, sizeof(int), cmp);
*其中cmp函数应写为：
*   int cmp(const void *a, const void *b)
*   {
*    return *(int*)a - *(int*)b; //由小到大排序
*    //return *(int *)b - *(int *)a; 由大到小排序
*   }
*/
void quick_sort(int * src,int left,int right)
{
    if(right>left)
    {
    int l,r,cent; 
    if(src==NULL)
    {
        return NULL;
    }
    l=left;
    r=right;
    cent=(l+r)/2;
    int a=src[cent];
    while(r>l)
    {
        while(r>cent && src[r]>=a)
            r--;
        if(r>cent)
        {
            src[cent]=src[r];
            cent++; 
            src[r]=src[cent];   //当左边为有序时，把中心点前面一个元素填充到检测的r位置重新检测
        }
        while(l<cent && src[l]<a)
            l++;
        if(l<cent)
        {
            src[r]=src[l];
            cent--;
            src[l]=src[cent];
        }
    }
    src[cent]=a;
    quick_sort(src,left,cent-1);
    quick_sort(src,cent+1,right);
    }
}


/* 冒泡排序 */
void bud_sort(int *src,int len)
{
    if(src==NULL || len<=1)
        return;
    int a=0;
    for (int i = 0; i < len-1; i++)
        for (int j = i; j < len; j++)
            if (src[j]>src[i])
            {
                a=src[i];
                src[i]=src[j];
                src[j]=a;
            }
}
/* 插入排序 */
void insert_sort(int *src,int len)
{
    int a;
    for (int i=1; i < len; i++)
    {
        if (src[i]<src[i-1])
        {
            a=src[i];
            int j;
            for (j=i-1; j>=0 && src[j] > a; j--)
            {
                src[j+1]=src[j];
            }
            src[j+1]=a;
            print_int(src,len);
        }
    }
    
}
/* 归并排序 */
void merge(int * sr,int * tr,int i,int m,int n)
{
    int j,k,l;
    for (j = m+1,k=i; i <=m && j<=n; k++)
    {
        if (sr[i]<sr[j])
            tr[k]=sr[i++];
        else
        {
            tr[k]=sr[j++];
        }
    }    
        if (i<=m)
        {
            for (l= 0; l<=m-i; l++)
            {
               tr[k+l]=sr[i+l];
            }
        }
        if (j<=n)
        {
            for (l= 0; l<=n-j; l++)
            {
                tr[k+l]=sr[j+l];
            }
            
        }
              
        
    
    
}
void msort(int *sr,int *tr1,int s,int t)
{
    int m;
    int i=0;
    while(i<=t-2*s)
    {
        merge(sr,tr1,i,i+s-1,i+2*s-1);
        i=i+2*s;
    }
    if (i<t-s)
    {
        merge(sr,tr1,i,i+s-1,t-1);
    }
    else
    {
        for(m=i;m<t;m++)
            tr1[m]=sr[m];
    }
    
    
}
void Mergesort(int *src,int len)
{
    int l=1;
    int *de=(int*)malloc(len*sizeof(int));
    while(l<len)
    {
        msort(src,de,l,len);
        // print_int(src,len);
        // printf("src=>");
        // print_int(de,len);
        l=2*l;
        msort(de,src,l,len);
        // print_int(de,len);
        // printf("de=>");
        // print_int(src,len);
        l=2*l;
    }
    free(de);
}

/* 堆排序 
*
*/
/* 以i，j设置一个大顶堆 */
void heap_adjust(int *src,int i,int j)
{
    int temp,a;
    temp=src[i];
    for ( a = 2*i+1; a<=j; a=a*2+1)
    {
        if (a<j && src[a]<src[a+1])
        {
            ++a;
        }
        if (temp>=src[a])
        {
            break;
        }
        src[i]=src[a];
        //保证移动后的堆每个结点依然是大顶堆
        i=a;
    }
    src[i]=temp;
}
void heap_sort(int *src,int len)
{
    int a;
    for (a=len/2;a >=0; a--)
    {
        heap_adjust(src,a,len-1); //从倒数第二层的每个结点开始构建大堆顶
    }
    for (a=len-1; a >0; a--)
    {
        int c;
        c=src[a];
        src[a]=src[0];
        src[0]=c;
        heap_adjust(src,0,a-1);
    }
    
    
}



/* 希尔排序 
*插入排序的优化，简单来说就是以无序序列插入有序序列中，把插入排序中以单个元素的插入升级为多个元素的序列插入 
*/
void shell_sort(int* src,int len)
{
    int i,j;
    int increment=len;
    int a;
    do
    {
        increment=increment/3+1;
        for ( i = increment ; i <len; i++)
        {
            if (src[i]<src[i-increment])
            {
                a=src[i];
                for ( j = i-increment; j>=0 && a < src[j]; j-=increment)
                {
                    src[j+increment]=src[j];
                }
                src[j+increment]=a;
            }
            
        }
    } while (increment >1);
    
}
// #define test_sort
#ifdef test_sort
int main(int argc, char const *argv[]) {
    
    int test1[]={2,35,67,3235,8,68,32,43,3};
    int test2[]={3,5326,675,235,7,8,436,23};
    int test3[]={325,53536,7,8,3227,8,9,6};
    int test4[]={3,4,5,352,-3,0,2};

    //快排测试
    /* 
    quick_sort(test1,0,sizeof(test1)/sizeof(int)-1);
    print_int(test1,sizeof(test1)/sizeof(int));

    quick_sort(test2,0,sizeof(test2)/sizeof(int)-1);
    print_int(test2,sizeof(test2)/sizeof(int));
    quick_sort(test3,0,sizeof(test3)/sizeof(int)-1);
    print_int(test3,sizeof(test3)/sizeof(int));
    quick_sort(test4,0,sizeof(test4)/sizeof(int)-1);
    print_int(test4,sizeof(test4)/sizeof(int)); 
    */

    //冒泡测试
    Mergesort(test1,sizeof(test1)/sizeof(int));
    print_int(test1,sizeof(test1)/sizeof(int));

    Mergesort(test2,sizeof(test2)/sizeof(int));
    print_int(test2,sizeof(test2)/sizeof(int));
    Mergesort(test3,sizeof(test3)/sizeof(int));
    print_int(test3,sizeof(test3)/sizeof(int));
    Mergesort(test4,sizeof(test4)/sizeof(int));
    print_int(test4,sizeof(test4)/sizeof(int));


    
    return 0;
}
#endif