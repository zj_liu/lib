/*
 * @Description: 
 * @Date: 2020-05-05 14:05:20
 * @LastEditors: LZJ
 * @LastEditTime: 2020-05-27 16:39:59
 * @FilePath: /src/tree/td_tree.c
 */
/***********************************************Include***********************************************/
#include <stdio.h>
#include <stdlib.h>
#include "td_stack.h"
#include "td_type.h"
/***********************************Global Definitions and Declarations*******************************/
/***********************************Constant / Macro Definitions**************************************/
typedef struct td_tree_node
{
    struct td_tree_node * right;
    struct td_tree_node * left;
    struct td_tree_node * parent;
    int k;
}TREE_NODE_T,*TREE_NODE_P;

/* 二叉查找树 */
typedef struct td_tree
{
    TREE_NODE_P top; //根节点

    int node;//节点数
    
    /* 查询 */
    TREE_NODE_P (*find)(struct td_tree *const me,int value);
    /* 添加一个节点 */
    TD_STATUS (*add)(struct td_tree* const me,TREE_NODE_P node);
	/* 从树中移除一个元素 并释放该元素的内存*/
	TD_STATUS (*remove)(struct td_tree* const me,int value);
	/* 清空树并释放内存 */
	TD_STATUS (*free)(struct td_tree* const me);
	/* 获取树中的元素个数 */
	int	(*count)(struct td_tree* const me);
}TREE_T,*TREE_P;

/*******************************************Type Declarations*****************************************/
/******************************************Function Declarations**************************************/
PRIVATE void td_tree_init(
						TREE_P const me,
                        TREE_NODE_P (*find_func)(TREE_P const me,int value),
						TD_STATUS (*add_func)(TREE_P const me,TREE_NODE_P node),
						TD_STATUS (*remove_func)(TREE_P const me,int value),
						TD_STATUS (*free_func)(TREE_P const me),
						int	(*count_func)(TREE_P const me)
						);
/* Operations */
PRIVATE  TREE_NODE_P td_tree_find(TREE_P const me,int value);
PRIVATE  TD_STATUS	 td_tree_add(TREE_P const me,TREE_NODE_P node);
PRIVATE  TD_STATUS   td_node_remove(TREE_P const me,int value);
PRIVATE  TD_STATUS   td_tree_free(TREE_P const me);
PRIVATE  int		 td_tree_count(TREE_P const me);

PRIVATE void td_tree_cleanup(TREE_P const me);
PUBLIC		TREE_P   td_tree_new(void);
PUBLIC		TD_STATUS	td_tree_delete(TREE_P const me);
/******************************************Variable Declarations**************************************/
/*******************************************Function Prototype****************************************/
PUBLIC TREE_P td_tree_new(void)
{
    TREE_P me=(TREE_P)malloc(sizeof(TREE_T));
    if (me!=NULL)
    {
        td_tree_init(me,
                    td_tree_find,
                    td_tree_add,
                    td_node_remove,
                    td_tree_free,
                    td_tree_count
                    );
    }
    return me;
}
PUBLIC TD_STATUS td_tree_delete(TREE_P const me)
{
    if (me!=NULL)
    {
        td_tree_cleanup(me);
        free(me);
        return TD_OK;
    }
    else
    {
        return TD_ERR_LIST_IS_NULL;
    }
}


PRIVATE void td_tree_init(
						TREE_P const me,
                        TREE_NODE_P (*find_func)(TREE_P const me,int value),
						TD_STATUS (*add_func)(TREE_P const me,TREE_NODE_P node),
						TD_STATUS (*remove_func)(TREE_P const me,int value),
						TD_STATUS (*free_func)(TREE_P const me),
						int	(*count_func)(TREE_P const me)
						)
{
    me->top=NULL;
    me->node=0;

    me->find=find_func;
    me->add=add_func;
    me->remove=remove_func;
    me->free=free_func;
    me->count=count_func;
}







PRIVATE TREE_NODE_P td_tree_find(TREE_P const me,int value)
{
    TREE_NODE_P node;
    node=me->top;
    while (node!=NULL && node->k != value)
    {
        if (node->k > value)
            node=node->left;
        else
            node=node->right;
    }
    return node;   
}


PRIVATE TD_STATUS td_tree_add(TREE_P const me,TREE_NODE_P node)
{
    TREE_NODE_P top;
    TREE_NODE_P parent;
    top=me->top;
    if (me->node==0)
    {
        me->top=node;
        node->parent=NULL;
        node->right=NULL;
        node->left=NULL;
        me->node++;
        return TD_OK;
    }
    while (top!=NULL)
    {
        parent=top;
        if (node->k > top->k)
        {
            top=top->right;
        }
        else if (node->k < top->k)
        {
            top=top->left;
        }
        else
        {
            return TD_ERR_TREE_NODE_IS_EXIT;
        }
    }
    node->parent=parent;
    node->left=NULL;
    node->right=NULL;
    if (parent->k > node->k)
    {
        parent->left=node;
    }
    else
    {
        parent->right=node;
    }
    me->node++;
    return TD_OK;          
}
PRIVATE TD_STATUS td_node_remove(TREE_P const me,int value)
{
    TREE_NODE_P node;
    int flag=0;//0代表该节点处于父节点的右子树
    node=td_tree_find(me,value);
    if (me==NULL || me->node==0 || node==NULL)
    {
        return TD_ERR_NODE_NOT_FOUND;
    }
    //获取删除节点处于父节点的位置（左还是右）
    if(node!=me->top &&  node->parent->k >value)
        flag=1;
    //删除节点是树叶
    if (node->left==NULL && node->right==NULL)
    {
        //如果删除的是根节点
        if(node==me->top)
        {
            me->top=NULL;
            goto end;
        }            
        else if (!flag)
        {
            node->parent->right=NULL;
        }
        else
        {
            node->parent->left=NULL;
            
        }
        goto end;
    }
    //有一个子树
    if(node->left==NULL)
    {
        //如果删除的是根节点
        if (node==me->top)
        {
            node->right->parent=NULL;
            me->top=node->right;
            goto end;
        }
        else if (!flag)
        {
            node->parent->right=node->right;
        }
        else
        {
            node->parent->left=node->right;
        }
        goto end;
    }
    if(node->right==NULL)
    {
        if (node==me->top)
        {
            node->left->parent=NULL;
            me->top=node->left;
            goto end;   
        }
        else if (!flag)
        {
            node->parent->right=node->left;
        }
        else
        {
            node->parent->left=node->left;
        }
        goto end;
    }

    //有二个子树,用待删除节点的右子树中的最小节点代替
    TREE_NODE_P parent;
    parent=node->right;
    // 获取右子树中的最小值元素
    while (parent->left!=NULL)
    {
        parent=parent->left;
    }
    //如果删除元素为根节点
    if(node==me->top)
    {   
        //判断要最小节点如果为父节点的右子树，则该节点为根节点的右子树
        if (parent->right!=NULL && flag==1)
        {
            parent->right->parent=parent->parent;
            parent->parent->left=parent->right;
        }
        
        parent->left=node->left;
        parent->parent=NULL;
        if(node->left!=NULL)
            node->left->parent=parent;
        me->top=parent;
        goto end;
    }

    if (!flag)
    {
        node->parent->right=parent;
    }
    else
    {
        node->parent->left=parent;
    }
    //如果最小节点是删除节点的右节点
    if (parent==node->right)
    {
        parent->left=node->left;
        parent->parent=node->parent;
        goto end;
    }
    //用右子树中最小值替换删除节点
    parent->parent->left=NULL;
    parent->parent=node->parent;
    parent->left=node->left;
    parent->right=node->right;
    goto end;
end:
    node->left=NULL;
    node->right=NULL;
    free(node);
    node=NULL;
    me->node--;
    return TD_OK;
}

PRIVATE  int td_tree_count(TREE_P const me)
{
    return me->node;
}
PRIVATE void td_tree_cleanup(TREE_P const me)
{

	me->free(me);
	
}
/* 递归清空树 */
PRIVATE void delete_tree(TREE_NODE_P node)
{
    TREE_NODE_P me;
    me=node;
    if(me==NULL)
        return ;
    if (me->left !=NULL)
        delete_tree(me->left);
    if(me->right !=NULL)
        delete_tree(me->right);
    free(me);
}
PRIVATE  TD_STATUS   td_tree_free(TREE_P const me)
{
    TREE_NODE_P node,left,right;
    if (me==NULL)
    {
        return TD_ERR_LIST_IS_NULL;
    }
    node = me->top;
    if(node !=NULL)
	{
        delete_tree(node);
		me->node = 0;
		me->top= NULL;
		return TD_OK;
	}
	else
	{
		return (TD_ERR_LIST_IS_NULL);
	}    
}


/* 遍历树 */

// 前序遍历
PRIVATE TD_STATUS PreTraverse(TREE_NODE_P node)
{
    if (node!=NULL)
    {
        printf("key：%d\n",node->k);
        if (NULL != node->left )
            PreTraverse(node->left);
        if (NULL != node->right )
            PreTraverse(node->right);
    }
    else
    {
        return TD_ERR_LIST_IS_NULL;
    }
}
// 中序遍历
PRIVATE TD_STATUS InTraverse(TREE_NODE_P node)
{
    if (node!=NULL)
    {
        
        if (NULL != node->left )
            PreTraverse(node->left);
        printf("key：%d\n",node->k);
        if (NULL != node->right )
            PreTraverse(node->right);
    }
    else
    {
        return TD_ERR_LIST_IS_NULL;
    }
}
// 后序遍历
PRIVATE TD_STATUS PostTraverse(TREE_NODE_P node)
{
    if (node!=NULL)
    {
        if (NULL != node->left )
            PreTraverse(node->left);
        if (NULL != node->right )
            PreTraverse(node->right); 
        printf("key：%d\n",node->k);
    }
    else
    {
        return TD_ERR_LIST_IS_NULL;
    }
}
/*Demo*/
// #define tree_demo
#ifdef tree_demo

typedef struct my_node
{
    TD_STACK_NODE_T node;
    TREE_NODE_P  t_node;
}MY_NODE_T,*MY_NODE_P;


TD_STACK_NODE_P new_node(TREE_NODE_P node)
{
    MY_NODE_P me;
    me=(MY_NODE_P)malloc(sizeof(MY_NODE_T));
    me->t_node=node;
    return (TD_STACK_NODE_P)me;
}
/* 深度优先遍历 */
void dfs(TREE_P const me)
{
    TD_STACK_P stack;
    TREE_NODE_P root;
    MY_NODE_P node=NULL;
    stack=td_stack_new();
    if (me->count==0)
    {
        return ;
    }
    
    root=me->top;

    stack->push(stack,new_node(root));

    while (stack->head!=NULL)
    {
        node=(MY_NODE_P)( stack->pop(stack) );
        printf("%d\n",node->t_node->k);
        if (node->t_node->right!=NULL)
        {
            stack->push(stack,new_node(node->t_node->right));
        }
        if (node->t_node->left!=NULL)
        {
            stack->push(stack,new_node(node->t_node->left));
        }
        node->t_node=NULL;
        free(node);
    }
    stack->free(stack);
}
/* 广度优先遍历*/
int main(int argc, char const *argv[])
{
    TREE_P tree;
    TREE_NODE_P p[10];
    tree=td_tree_new();
    for (int i = 0; i < 10; i++)
    {
        p[i]=(TREE_NODE_P)malloc(sizeof(TREE_NODE_T));
        p[i]->k=i%2 ? i:-i;
        tree->add(tree,p[i]);
    }
    
    printf("tree:%d\n",tree->top->k);
    dfs(tree);
    // PreTraverse(tree->top);
    //InTraverse(tree->top);
    // PostTraverse(tree->top);
    printf("count=%d\n",tree->node);
    for (int i = 0; i < 10; i++)
    {
        tree->remove(tree,p[i]->k);
        PreTraverse(tree->top);
        // InTraverse(tree->top);
        // PostTraverse(tree->top);
        printf("count=%d\n",tree->node);
    }
    printf("count=%d\n",p[2]->k);
    tree->free(tree);
    return 0;
}
#endif