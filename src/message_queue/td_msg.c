/*
 * @Description: 
 * @Date: 2020-05-04 13:34:42
 * @LastEditors: LZJ
 * @LastEditTime: 2020-05-12 08:30:23
 * @FilePath: /src/message_queue/td_msg.c
 */
/***********************************************Include***********************************************/
#include <stdio.h>
#include <sys/ipc.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include "log.h"
#include "td_type.h"
#include "td_msg.h"
/***********************************Global Definitions and Declarations*******************************/
/***********************************Constant / Macro Definitions**************************************/
/*******************************************Type Declarations*****************************************/
/******************************************Function Declarations**************************************/
/******************************************Variable Declarations**************************************/
static int log_lever=2;
/*******************************************Function Prototype****************************************/

PRIVATE char* td_msg_allocate(struct td_msg_body* me,int length)	//request memory for buf
{
	
	if(length>MAX_MSG_LEN)
	{
		LOG(LOG_DEBUG,"warnning length(%d) too long!\r\n",length);
		return NULL;
	}
	
	if(me->buf !=NULL)
	{
		LOG(LOG_DEBUG,"warning: buf is not NULL, may cause memory leak!\r\n");
		return NULL;
	}
	
	me->buf = malloc(length);
	if(me->buf!=NULL)
	{
		me->length = length;
	}
	else
	{
		me->length = 0;
	}
	
	return me->buf;
}


//Free the application memory buf,set length = 0  type = 0
PRIVATE int td_msg_free(struct td_msg_body* me)	
{
	//LOG("me->buf=%x\r\n",me->buf);
	if(me->buf!=NULL)
	{
		free(me->buf);
		me->buf  = NULL;
	}
	me->type = 0;
	me->length = 0;
	
	return 0;
}

PRIVATE int td_msg_init(
						TD_MSG_P const me,
						int type,char *dat,int length,
						char* (*allocate_func)(struct td_msg_body* me,int length),
						int (*free_func)(struct td_msg_body* me)
						)
{
	me->allocate = allocate_func;
	me->free = free_func;
	
	me->type = 0;
	me->length = 0;
	me->buf = NULL;
	
	if(length!=0)
	{
		me->buf = me->allocate(me,length);
		if(me->buf==NULL)
		{
			return -1;
		}
		LOG(LOG_DEBUG,"dat = %s\r\n",dat);
		memcpy(me->buf,dat,length);
		LOG(LOG_DEBUG,"me->buf = %s\r\n",me->buf);
		me->type = type;
	}

	return 0;
}

int td_msg_cleanup(const TD_MSG_P me)
{
	if(me!=NULL)
	{
		me->free(me);
	}
	return 0;
}
	
/************************************************************************
*                F U N C T I O N   D E F I N I T I O N S
*************************************************************************
*/

/*=======================================================================
* PUBLIC FUNCTIONS
**=======================================================================
*/

/*-----------------------------------------------------------------------
  FUNCTION: td_msg_new
-------------------------------------------------------------------------
/// <summary>

/// </summary>

[PARAMETERS]
type: The message type cannot be 0
[RETURN]
*
*/
PUBLIC TD_MSG_P td_msg_new(int type,char *dat,int length)
{
	int ret;
	TD_MSG_P me = NULL;
	
	me = (TD_MSG_P)malloc(sizeof(TD_MSG_T));
	
	if(me!=NULL)
	{
		ret = td_msg_init(me,type,dat,length,
						 td_msg_allocate,
						 td_msg_free
					 );
		if(ret != 0)
		{
			LOG(LOG_DEBUG,"td_msg_init failed!\r\n");
			free(me);
			me = NULL;
		}
	}

#ifdef MEMORY_LEAK_CHECK
	msg_count++;
#endif	
	
	return (me);
}



/*-----------------------------------------------------------------------
  FUNCTION: td_msg_delete
-------------------------------------------------------------------------
/// <summary>
This function delete a td_list object.
If the list is not empty, a list free operation will be excuted and 
it will frees up memory used for nodes.
Finally it frees up memory used for the list object.
/// </summary>

[PARAMETERS]
[RETURN]
*
*/
PUBLIC int 	td_msg_delete(TD_MSG_P me)
{

#ifdef MEMORY_LEAK_CHECK
	if(msg_count == 0)
	{
		LOG(LOG_DEBUG,"WARNING!!!!!!!!!!!!!!!!function:%s,    line:%d\r\n",__FUNCTION__,__LINE__);
	}
	else
	{
		msg_count--;
	}
#endif


	if(me!=NULL)
	{
		/* NORMALIZED FORMAT OF A DELETE OPRETAION 
		call td_list_cleanup routine to do some cleanups.
		then free the object. */
		td_msg_cleanup(me);
		free(me);
		me = NULL;
	}
	return (0);
}

/*=======================================================================
* PRIVATE FUNCTIONS
**=======================================================================
*/
//Creating message queues
//If the queue does not exist, create, fail to return -1
//f the queue exists, set the me->id to the queue id
PRIVATE int td_msg_q_create(struct td_msg_queue_tag* me)
{
	int msqid;
	int n=10;
	do
	{ 
        LOG(LOG_DEBUG,"me->key=%d\r\n",me->key);
		msqid=msgget(me->key,IPC_EXCL);  /*Check if message queues exist*/  
		if(msqid < 0)
		{  
			msqid = msgget(me->key,IPC_CREAT|0666);/*Creating message queues*/  
			if(msqid <0)
			{  
				LOG(LOG_DEBUG,"failed to create msq | errno=%d [%s]\n",errno,strerror(errno)); 
				return -1;
			}
			else
			{
				me->id = msqid;
				LOG(LOG_DEBUG,"td_msg_q_create ok, id = %d\r\n",me->id);			
				return 0;
			}
		}
		else
		{
			//me->key++;
			me->id = msqid;
			LOG(LOG_DEBUG,">>>>>>>>>>>>>>>>>>>queue already exits! id = %d\r\n",me->id);
			msgctl(me->id,IPC_RMID,0); //Delete message queue
		}
		
	}while(n--);
	
	
	return -1;
}

//Delete message queue
PRIVATE int td_msg_q_destroy(struct td_msg_queue_tag* me)
{	
	if(me->id >= 0)
	{
		msgctl(me->id,IPC_RMID,0); //Delete message queue
		me->id = -1;
	}
	return 0;
}



//Send a message to the message queue
PRIVATE int td_msg_q_post( struct td_msg_queue_tag* me,TD_MSG_P pmsg)
{
	int ret;
	int msgid;
	
	if(me->id<0)
	{
		return -1;
	}
	
	if(pmsg->length==0)
	{
		//return -2;
	}
	
	msgid = msgget(me->key,IPC_EXCL);	/*Check if message queues exist */  
	if(msgid < 0)
	{  
		LOG(LOG_DEBUG,"msq not existed! errno=%d [%s]\n",errno,strerror(errno));
		syslog("msq not existed! errno=%d [%s]\n",errno,strerror(errno));
		return -3;
	}
	
	if(me->id != msgid)
	{
		LOG(LOG_DEBUG,"msg queue id not match!\r\n");
		syslog("msg queue id not match!\r\n");
		return -4;
	}
	
	/* Send message to the queue*/  
	ret = msgsnd(me->id,pmsg,12,IPC_NOWAIT);//12为length+buf的大小,该函数是阻塞函数
	if ( ret < 0 ) 
	{  
	    
		//LOG("msgsnd()ret:%d write msg failed,errno=%d[%s]\n",ret,errno,strerror(errno));  
		syslog("msgsnd()me->id:%d ret:%d write msg failed,errno=%d[%s]\n",me->id,ret,errno,strerror(errno));  
        exit(EXIT_FAILURE);
		return -5;
	}
	// pmsg->buf = NULL; 		//TODO:The message buf is supposed to be sent to NULL, which is equivalent to removing the message from the sender
	pmsg->length = 0;
	pmsg->type = 0;
	
	//td_msg_delete(pmsg);	//
	return 0;
}

//Receive messages from the message queue and save them to the 'pmsg'
PRIVATE int td_msg_q_pend( struct td_msg_queue_tag* me,TD_MSG_P pmsg)
{
	int ret;
	int msgid;
	
	if(me->id<0)
	{
		return -1;
	}
	
	msgid = msgget(me->key,IPC_EXCL);	/* check if message queue exists */
	if(msgid < 0)
	{  
		LOG(LOG_DEBUG,"msq not existed! errno=%d [%s]\n",errno,strerror(errno));  
		return -2;
	}
	
	if(me->id != msgid)
	{
		LOG(LOG_DEBUG,"msg queue id not match!\r\n");
		return -3;
	}
	
	/* Receive message from the queue*/  
	ret = msgrcv(msgid,pmsg,MAX_MSG_LEN,0,0); 
	//LOG("td_msg_q_pend ret = %d\r\n",ret);
	
	return ret; //Returns the actual message length received
}
	

PRIVATE int td_msg_queue_init(const TD_MSG_Q_P me,int key,
							  int (*create_func)( struct td_msg_queue_tag* me),
							  int (*destroy_func)( struct td_msg_queue_tag* me),
							  int (*post_func)( struct td_msg_queue_tag* me,TD_MSG_P pmsg),
							  int (*pend_func)( struct td_msg_queue_tag* me,TD_MSG_P pmsg)
							 )
{
	me->create = create_func;
	me->destroy = destroy_func;
	me->post = post_func;
	me->pend = pend_func;
	
	me->key = key;
	me->id = -1;
	
	return 0;
}

/*-----------------------------------------------------------------------
  FUNCTION: td_list_init
-------------------------------------------------------------------------
/// <summary>

/// </summary>
[PARAMETERS]
[RETURN]
*
*/
PUBLIC TD_MSG_Q_P 	td_msg_queue_new(int key)
{
	int ret;
	TD_MSG_Q_P me = NULL;

	LOG(LOG_DEBUG,"td_msg_queue_new...\r\n");

	me = (TD_MSG_Q_P)malloc(sizeof(TD_MSG_Q_T));

	LOG(LOG_DEBUG,"td_msg_queue_new...ok\r\n");
	
	if(me!=NULL)
	{
		ret = td_msg_queue_init(me,key,
								td_msg_q_create,
								td_msg_q_destroy,
								td_msg_q_post,
								td_msg_q_pend
					 );
		if(ret != 0)
		{
			syslog("td_msg_init failed!\r\n");
			free(me);
			me = NULL;
		}
	}
	
	return (me);
	
}

PRIVATE int td_msg_queue_cleanup(TD_MSG_Q_P me)
{
	me->key = 0;
	me->id = -1;

	return 0;
}

PUBLIC int 	td_msg_queue_delete(TD_MSG_Q_P me)
{
	if(me!=NULL)
	{
		td_msg_queue_cleanup(me);
		free(me);
		me = NULL;
	}
	return 0;
}


/* Demo */
// #define test_msg
#ifdef test_msg

int main(int argc, char const *argv[])
{
    int ret=-1;
    TD_MSG_P test=NULL,test_recv=NULL;
    TD_MSG_Q_P test1;
    test=td_msg_new(1,"12345678910111213",strlen("12345678910111213"));
    test->type=1;
    // printf("head:%p,type:%p,len:%p,buf:%p,f1:%p,f2:%p\n",test,&test->type,&test->length,\
	&(test->buf),test->allocate,test->free);
    test_recv = td_msg_new(1,NULL,0);
    
    test1=td_msg_queue_new(10);
    test1->create(test1);
    test1->post(test1,test);
    while (1)
    {
        /* code */
        ret=test1->pend(test1,test_recv);
        if(ret>0)
        {
            printf("recv:%s\n",test_recv->buf);
            test_recv->free(test_recv);
            break;
        }
            
    }
    
    return 0;
}



#endif