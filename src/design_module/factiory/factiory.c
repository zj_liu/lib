/*
 * @Description: 
 * @Date: 2020-04-28 12:18:50
 * @LastEditors: LZJ
 * @LastEditTime: 2020-05-12 08:30:56
 * @FilePath: /src/design_module/factiory/factiory.c
 */
/***********************************************Include***********************************************/
#include <stdio.h>
/***********************************Global Definitions and Declarations*******************************/
typedef enum factory_product_type
{
    ADD,
    SUB,
}FACTORY_PRODUCT_TYPE;

typedef struct factory_product
{
    double (*getresult)(struct  factory_product *const me);
}PRODUCT_T,*PRODUCT_P;

typedef struct factory
{
    PRODUCT_P (*creat_product)(FACTORY_PRODUCT_TYPE type);
}FACTORY_T,*FACTORY_P;


typedef struct product_add
{
    PRODUCT_T pr;
    
    double a;
    double b;
    double result;
    
}add_t,*add_p;


/***********************************Constant / Macro Definitions**************************************/
/*******************************************Type Declarations*****************************************/
/******************************************Function Declarations**************************************/
/******************************************Variable Declarations**************************************/
/*******************************************Function Prototype****************************************/
static double add(PRODUCT_P const me)
{
    return ((add_p)me)->a+((add_p)me)->b;
}
static PRODUCT_P creat_product(FACTORY_PRODUCT_TYPE type)
{
    PRODUCT_P product;
    
    switch (type)
    {
    case ADD:
        product=(PRODUCT_P)malloc(sizeof(add_t));
        product->getresult=add;
        break;
    default:
        break;
    }
    return product;
}
static  factory_init(FACTORY_P const me,PRODUCT_P (*creat_product)(FACTORY_PRODUCT_TYPE type))
{
        me->creat_product=creat_product;
        return me;
}
FACTORY_P new_factory()
{
    FACTORY_P fac;
    fac=(FACTORY_P)malloc(sizeof(FACTORY_T));
    factory_init(fac,creat_product);
    return fac;
}
void factior_cleanup(FACTORY_P const me)
{
    if(me!=NULL)
        free(me);
}
void product_cleanup(PRODUCT_P const me)
{
    if(me!=NULL)
        free(me);
}
/* Demo */
#ifdef test_fac
int main(int argc, char const *argv[])
{
    FACTORY_P fac;
    add_p p1;
    fac=new_factory();
    p1=fac->creat_product(ADD);
    p1->a=1.156469;
    p1->b=2.1161;
    double res=p1->pr.getresult((PRODUCT_P)p1);
    printf("res=%f\n",res);
    product_cleanup(p1);
    factior_cleanup(fac);
    return 0;
}
#endif