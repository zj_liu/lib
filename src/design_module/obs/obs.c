/*
 * @Description: 
 * @Date: 2020-05-27 14:42:35
 * @LastEditors: LZJ
 * @LastEditTime: 2020-05-27 18:37:42
 * @FilePath: /src/design_module/obs/obs.c
 */ 

/***********************************************Include***********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "td_list.h"
#include "td_type.h"
/***********************************Global Definitions and Declarations*******************************/
/***********************************Constant / Macro Definitions**************************************/
/*******************************************Type Declarations*****************************************/

typedef struct obs obs_T,*obs_P;
typedef enum obs_STATS
{
    INITVALUE,
    Test
}OBS_STATE;


typedef struct sub
{
    /* data */
    TD_LIST_P obs_list;
    OBS_STATE state;
    void (*add_attach)(struct sub * const me, obs_P node);
    void (*remove_attach)(struct sub * const me, obs_P node);
    void (*notifyAll)(struct sub * const me);
    OBS_STATE (*get_state)(struct sub * const me);
    void (*set_state)(struct sub * const me,OBS_STATE state);
}sub_T,*sub_P;



typedef struct obs
{
    TD_NODE_T node;
    
    sub_P sub;
    void (*update)(sub_P me);
}obs_T,*obs_P;


/******************************************Function Declarations**************************************/
PRIVATE add_attach_fun(sub_P const me, obs_P node);
PRIVATE remove_attach_fun(sub_P const me, obs_P node);
PRIVATE notifyAll_fun(sub_P const me);
PRIVATE OBS_STATE get_state(sub_P const me);
PRIVATE set_state(sub_P const me,OBS_STATE state);
PRIVATE sub_init(sub_P const me,OBS_STATE state,
                        void (*add_attach)(sub_P const me, obs_P node),
                        void (*remove_attach)(sub_P const me, obs_P node),
                        void (*notifyAll)(sub_P const me),
                        OBS_STATE (*get_state)(sub_P const me),
                        void (*set_state)(sub_P const me,OBS_STATE state));
PRIVATE sub_cleanup(sub_P const me);

/******************************************Variable Declarations**************************************/
/*******************************************Function Prototype****************************************/

PUBLIC sub_P new_sub()
{
    sub_P me=(sub_P)malloc(sizeof(sub_T));
    if (me!=NULL)
        sub_init(me,INITVALUE,add_attach_fun,remove_attach_fun,notifyAll_fun,get_state,set_state);
    return me;
}
PRIVATE sub_init(sub_P const me,OBS_STATE state,
                        void (*add_attach)(sub_P const me, obs_P node),
                        void (*remove_attach)(sub_P const me, obs_P node),
                        void (*notifyAll)(sub_P const me),
                        OBS_STATE (*get_state)(sub_P const me),
                        void (*set_state)(sub_P const me,OBS_STATE state))
{
    me->state=state;
    me->obs_list=td_list_new();

    me->add_attach=add_attach_fun;
    me->remove_attach=remove_attach_fun;
    me->notifyAll=notifyAll_fun;
    me->get_state=get_state;
    me->set_state=set_state;
}

PRIVATE add_attach_fun(sub_P const me, obs_P node)
{
    if (me!=NULL)
    {
        me->obs_list->add(me->obs_list,(TD_NODE_P)node);
    }
    
}
PRIVATE remove_attach_fun(sub_P const me, obs_P node)
{
    if (me!=NULL && me->obs_list!=NULL)
    {
        me->obs_list->remove(me->obs_list,(TD_NODE_P)node);
    }
}
PRIVATE notifyAll_fun(sub_P const me)
{
    TD_NODE_P node;
    node=me->obs_list->head;
    while (node!=NULL)
    {
        ((obs_P)node)->update(me);
        node=node->next;
    }    
}

PRIVATE sub_cleanup(sub_P const me)
{
    if (me!=NULL)
    {
        free(me);
    }
}
PRIVATE OBS_STATE get_state(sub_P const me)
{
    return me->state;
}
PRIVATE set_state(sub_P const me,OBS_STATE state)
{
    me->state=state;
    me->notifyAll(me);
}
/* 
brief: 只是清空观察者列表，并不会释放其内存
 */
PUBLIC sub_delete(sub_P const me )
{
    if(me!=NULL)
    {
        if (me->obs_list!=NULL)
        {
            TD_NODE_P obs_node=me->obs_list->head;
            TD_NODE_P obs_next;
            while(obs_node!=NULL)
                {
                    obs_next=obs_node->next;
                    me->remove_attach(me,obs_node);
                    obs_node=obs_next;
                }
            me->obs_list=NULL;  
        }
        sub_cleanup(me);
    }
}






// #define obs_demo
#ifdef obs_demo

typedef struct obs1
{
    obs_T subject;
    /* data */
}obs1_T,*obs1_P;

typedef struct obs2
{
    obs_T subject;
    /* data */
}obs2_T,*obs2_P;


void update1(sub_P me)
{
    printf("state=%d\n",me->state);
}

obs1_P new_obs1(sub_P const subject,void(*update)())
{
    obs1_P me=(obs1_P)malloc(sizeof(obs1_T));
    me->subject.sub=subject;
    me->subject.update=update;
    
    subject->add_attach(subject,(obs_P)me);
    
    return me;

}




int main(int argc, char const *argv[]) {
    obs1_P me;
    sub_P subject;
    subject=new_sub();
    me=new_obs1(subject,update1);
    subject->set_state(subject,1);
    sub_delete(subject);
    subject=NULL;
    free(me);
    return 0;
}
#endif