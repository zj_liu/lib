/*
 * @Description: 
 * @Date: 2020-04-26 14:50:31
 * @LastEditors: LZJ
 * @LastEditTime: 2020-05-04 13:36:15
 * @FilePath: /src/queue/queue.c
 */
/***********************************************Include***********************************************/
#include "td_type.h"
#include "td_queue.h"
/***********************************Global Definitions and Declarations*******************************/

/***********************************Constant / Macro Definitions**************************************/
/*******************************************Type Declarations*****************************************/
/******************************************Variable Declarations**************************************/
/*******************************************Function Prototype****************************************/
PRIVATE void td_queue_init(
                        TD_QUEUE_P const me,
                        TD_QUEUE_NODE_P (*get_front_node)   (TD_QUEUE_P const me),
                        TD_QUEUE_NODE_P (*get_rear_node)   (TD_QUEUE_P const me),
						TD_STATUS (*EnQueue_func)     (TD_QUEUE_P const me,TD_QUEUE_NODE_P node),
						TD_STATUS (*DeQueue_func)     (TD_QUEUE_P const me),
						TD_STATUS (*free_func)        (TD_QUEUE_P const me),
						int	(*count_func)             (TD_QUEUE_P const me)
                      );

PRIVATE TD_QUEUE_NODE_P td_queue_get_front_node(TD_QUEUE_P const me);                      
PRIVATE TD_QUEUE_NODE_P td_queue_get_rear_node(TD_QUEUE_P const me);
PRIVATE TD_STATUS td_queue_EnQueue_func(TD_QUEUE_P const me,TD_QUEUE_NODE_P node);
PRIVATE TD_STATUS td_queue_DeQueue_func(TD_QUEUE_P const me);
PRIVATE int td_queue_count(TD_QUEUE_P const me);
PRIVATE TD_STATUS td_queue_free(TD_QUEUE_P const me);

PRIVATE void td_queue_cleanup(TD_QUEUE_P const me);
PUBLIC TD_QUEUE_P td_queue_new(void);
PUBLIC TD_STATUS  td_queue_delete(TD_QUEUE_P const me);

PUBLIC TD_QUEUE_P td_queue_new(void)
{
    TD_QUEUE_P me = (TD_QUEUE_P)malloc(sizeof(TD_QUEUE_T));
    if(me!=NULL)
    {
        td_queue_init(me,
                      td_queue_get_front_node,
                      td_queue_get_rear_node,
                      td_queue_EnQueue_func,
                      td_queue_DeQueue_func,
                      td_queue_free,
                      td_queue_count
                      );
    }
    return me;
}
PUBLIC TD_STATUS td_queue_delete(TD_QUEUE_P const me)
{
    if(me !=NULL)
    {
        td_queue_cleanup(me);
        free(me);
        return TD_OK;
    }
    else
    {
        return TD_ERR_LIST_IS_NULL;
    }
}

PRIVATE void td_queue_init(
                        TD_QUEUE_P const me,
                        TD_QUEUE_NODE_P (*get_front_node)   (TD_QUEUE_P const me),
                        TD_QUEUE_NODE_P (*get_rear_node)    (TD_QUEUE_P const me),
						TD_STATUS (*EnQueue_func)           (TD_QUEUE_P const me,TD_QUEUE_NODE_P node),
						TD_STATUS (*DeQueue_func)           (TD_QUEUE_P const me),
						TD_STATUS (*free_func)              (TD_QUEUE_P const me),
						int	(*count_func)                   (TD_QUEUE_P const me)
                      )
{
    /* initialize attributes */
    me->front=NULL;
    me->rear=NULL;
    me->n_node=0;

    /* initialize member function pointers */
    me->get_front=get_front_node;
    me->get_rear=get_rear_node;
    me->EnQueue=EnQueue_func;
    me->DeQueue=DeQueue_func;
    me->free=free_func;
    me->count=count_func;
} 
PRIVATE TD_QUEUE_NODE_P td_queue_get_front_node(TD_QUEUE_P const me)
{
    if(me->front !=NULL)
    {
        return me->front;
    }
    else
    {
        return NULL;
    }   
}
PRIVATE TD_QUEUE_NODE_P td_queue_get_rear_node(TD_QUEUE_P const me)
{
    if(me->rear !=NULL)
    {
        return me->rear;
    }
    else
    {
        return NULL;
    }   
}
PRIVATE TD_STATUS td_queue_EnQueue_func(TD_QUEUE_P const me,TD_QUEUE_NODE_P node)
{
    if (me==NULL)
    {
        return TD_ERR_STACK_IS_NULL;
    }
    if (me->n_node==0)
    {
        me->front=node;
        me->rear=node;
        node->next=NULL;
    }
    else
    {
        me->rear->next=node;
        node->next=NULL;
        me->rear=node;
    }
    me->n_node++;
}
PRIVATE TD_STATUS td_queue_DeQueue_func(TD_QUEUE_P const me)
{
    TD_QUEUE_NODE_P node;
    if (me ==NULL)
    {
       return TD_ERR_STACK_IS_NULL;
    }
    node=me->front;
    if (me->n_node==0)
    {
        return TD_ERR_STACK_EMPTY;
    }
    else
    {
        me->front=me->front->next;
        free(node);
    }
    me->n_node--;
    return TD_OK;
}
PRIVATE TD_STATUS td_queue_free(TD_QUEUE_P const me)
{
    TD_QUEUE_NODE_P node;
    if(me!=NULL){
        while(me->front!=NULL)
        {
            me->DeQueue(me);
        }
        me->n_node=0;
        me->front=NULL;
        me->rear=NULL;
    }
    else
    {
        return TD_ERR_LIST_IS_NULL;
    } 
}
PRIVATE int td_queue_count(TD_QUEUE_P const me)
{
    return me->n_node;
}
PRIVATE void td_queue_cleanup(TD_QUEUE_P const me)
{
	me->free(me);	
}


/* Demo */
// #define test_queu
#ifdef test_queu
TD_QUEUE_P queue;
typedef struct my_node
{
    TD_QUEUE_NODE_T node;
    int i;
}MY_NODE_T,*MY_NODE_P;
void print_queue(TD_QUEUE_P const me)
{
    TD_QUEUE_NODE_P node;
    if (me!=NULL)
    {
        node=me->front;
        if(node ==NULL)
        {
            printf("queue node in null\n");
            return;
        }
        while(node!=NULL)
        {
            printf("i=%d\n",((MY_NODE_P)node)->i);
            node=node->next;
        }
    }
    
}



int main(void) {
    MY_NODE_P node[10];
    queue=td_queue_new();
    for (int i = 0; i < 10; i++)
    {  
        node[i]=(MY_NODE_P)malloc(sizeof(MY_NODE_T));
        node[i]->i=i;
        queue->EnQueue(queue,(TD_QUEUE_NODE_P)node[i]);
    }
    print_queue(queue);
    for (int i = 0; i < 10; i++)
    {   
        queue->DeQueue(queue);
        print_queue(queue);
    }
    queue->free(queue);
    return 0;
}


#endif