/*
 * @Description: 
 * @Date: 2020-04-25 19:55:29
 * @LastEditors: LZJ
 * @LastEditTime: 2020-05-12 18:21:02
 * @FilePath: /src/stack/td_stack.c
 */

/***********************************************Include***********************************************/
#include "td_type.h"
#include "td_stack.h"
/***********************************Global Definitions and Declarations*******************************/
/***********************************Constant / Macro Definitions**************************************/
/*******************************************Type Declarations*****************************************/
/******************************************Variable Declarations**************************************/
/*******************************************Function Prototype****************************************/
PRIVATE void td_stack_init(
                        TD_STACK_P const me,
                        TD_STACK_NODE_P (*get_func)(TD_STACK_P const me),
						TD_STATUS (*push_func)(TD_STACK_P const me,TD_STACK_NODE_P node),
						TD_STACK_NODE_P (*pop_func)(TD_STACK_P const me),
						TD_STATUS (*free_func)(TD_STACK_P const me),
						int	(*count_func)(TD_STACK_P const me)
                      );

PRIVATE TD_STACK_NODE_P td_stack_get(TD_STACK_P const me);                      
PRIVATE TD_STATUS td_stack_push(TD_STACK_P const me,TD_STACK_NODE_P node);
PRIVATE TD_STACK_NODE_P td_stack_pop(TD_STACK_P const me);
PRIVATE TD_STATUS td_stack_free(TD_STACK_P const me);
PRIVATE int td_stack_count(TD_STACK_P const me);

PRIVATE void td_stack_cleanup(TD_STACK_P const me);
PUBLIC TD_STACK_P td_stack_new(void);
PUBLIC TD_STATUS  td_stack_delete(TD_STACK_P const me);

PUBLIC TD_STACK_P td_stack_new(void)
{
    TD_STACK_P me = (TD_STACK_P)malloc(sizeof(TD_STACK_T));
    if(me!=NULL)
    {
        td_stack_init(me,
                      td_stack_get,
                      td_stack_push,
                      td_stack_pop,
                      td_stack_free,
                      td_stack_count
                      );
    }
    return me;
}
PUBLIC TD_STATUS td_stack_delete(TD_STACK_P const me)
{
    if(me !=NULL)
    {
        td_stack_cleanup(me);
        free(me);
        return TD_OK;
    }
    else
    {
        return TD_ERR_LIST_IS_NULL;
    }
}

PRIVATE void td_stack_init(
                        TD_STACK_P const me,
                        TD_STACK_NODE_P (*get_func)(TD_STACK_P const me),
						TD_STATUS (*push_func)(TD_STACK_P const me,TD_STACK_NODE_P node),
						TD_STACK_NODE_P (*pop_func)(TD_STACK_P const me),
						TD_STATUS (*free_func)(TD_STACK_P const me),
						int	(*count_func)(TD_STACK_P const me)
                      )
{
    /* initialize attributes */
    me->head=NULL;
    me->n_node=0;

    /* initialize member function pointers */
    me->get=get_func;
    me->push=push_func;
    me->pop=pop_func;
    me->free=free_func;
    me->count=count_func;
} 
PRIVATE TD_STACK_NODE_P td_stack_get(TD_STACK_P const me)
{
    if(me->head !=NULL)
    {
        return me->head;
    }
    else
    {
        return NULL;
    }
    
}
PRIVATE TD_STATUS td_stack_push(TD_STACK_P const me,TD_STACK_NODE_P node)
{
    if (me==NULL)
    {
        return TD_ERR_STACK_IS_NULL;
    }
    if (me->n_node==0)
    {
        me->head=node;
        node->next=NULL;
    }
    else
    {
        node->next=me->head;
        me->head=node;
    }
    me->n_node++;
    return TD_OK;
}
PRIVATE TD_STACK_NODE_P td_stack_pop(TD_STACK_P const me)
{
    TD_STACK_NODE_P top;
    if (me ==NULL)
    {
       return NULL;
    }
    top=me->head;
    if (me->n_node==0)
    {
        return NULL;
    }
    else
    {
        me->head=me->head->next; 
        me->n_node--;
        return top;
    }
}
PRIVATE TD_STATUS td_stack_free(TD_STACK_P const me)
{
    TD_STACK_NODE_P node;
    if(me!=NULL){
        while(me->head!=NULL)
        {
            me->pop(me);
        }
        me->n_node=0;
        me->head=NULL;
    }
    else
    {
        return TD_ERR_LIST_IS_NULL;
    } 
}
PRIVATE int td_stack_count(TD_STACK_P const me)
{
    return me->n_node;
}
PRIVATE void td_stack_cleanup(TD_STACK_P const me)
{
	me->free(me);	
}



/* Demo */
// #define test

#ifdef test
TD_STACK_P stack;
typedef struct my_node
{
    TD_STACK_NODE_T node;
    int i;
}MY_NODE_T,*MY_NODE_P;
void print_stack(TD_STACK_P const me)
{
    TD_STACK_NODE_P node;
    if (me!=NULL)
    {
        node=me->head;
        if(node ==NULL)
        {
            printf("stack node in null\n");
            return;
        }
        while(node!=NULL)
        {
            printf("i=%d\n",((MY_NODE_P)node)->i);
            node=node->next;
        }
    }
    
}



int main(void) {
    MY_NODE_P node[10];
    stack=td_stack_new();
    for (int i = 0; i < 10; i++)
    {  
        node[i]=(MY_NODE_P)malloc(sizeof(MY_NODE_T));
        node[i]->i=i;
        stack->push(stack,(TD_STACK_NODE_P)node[i]);
    }
    print_stack(stack);
    for (int i = 0; i < 10; i++)
    {   MY_NODE_P n;
        n=(MY_NODE_P)stack->pop(stack);
        printf("pop:%d\n",n->i);
        print_stack(stack);
    }
    stack->free(stack);
    return 0;
}
#endif