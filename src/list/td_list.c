/*
 * @Author: LZJ
 * @since: 2020-04-18
 * @lastTime: 2020-04-25
 * @LastAuthor: Do not edit
 * @FilePath: \td_list.c
 * @Description: 
 */
/***********************************************Include***********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "td_list.h"
/***********************************Global Definitions and Declarations*******************************/

/***********************************Constant / Macro Definitions**************************************/
/*******************************************Type Declarations*****************************************/
/******************************************Variable Declarations**************************************/
/*******************************************Function Prototype****************************************/
/* Constructors and destructors */
PRIVATE void td_list_init(
						TD_LIST_P const me,
						TD_STATUS (*add_func)(TD_LIST_P const me,TD_NODE_P node),
						TD_STATUS (*remove_func)(TD_LIST_P const me,TD_NODE_P node),
						TD_STATUS (*insert_func)(TD_LIST_P const me,TD_NODE_P prev_node,TD_NODE_P node),
						TD_STATUS (*concat_func)(TD_LIST_P const me,TD_LIST_P const add_list),
						TD_STATUS (*extract_func)(TD_LIST_P const me,TD_NODE_P start_node,TD_NODE_P end_node,TD_LIST_P const dst_list),
						TD_STATUS (*free_func)(TD_LIST_P const me),
						int	(*count_func)(TD_LIST_P const me)
						);
/* Operations */
PRIVATE  TD_STATUS	td_list_add(TD_LIST_P const me,TD_NODE_P node);
PRIVATE  TD_STATUS  td_node_remove(TD_LIST_P const me,TD_NODE_P node);
PRIVATE  TD_STATUS  td_node_insert(TD_LIST_P const me,TD_NODE_P prev_node,TD_NODE_P node);
PRIVATE  TD_STATUS  td_list_concatenate(TD_LIST_P const me,TD_LIST_P const add_list);
PRIVATE  TD_STATUS  td_list_extract(TD_LIST_P const me,TD_NODE_P start_node,TD_NODE_P end_node,TD_LIST_P const dst_list);
PRIVATE  TD_STATUS  td_list_free(TD_LIST_P const me);
PRIVATE  int		td_list_count(TD_LIST_P const me);

PRIVATE void td_list_cleanup(TD_LIST_P const me);
PUBLIC		TD_LIST_P   td_list_new(void);
PUBLIC		TD_STATUS	td_list_delete(TD_LIST_P const me);
/***************************************************************************************************************************/
PUBLIC TD_LIST_P  td_list_new(void)
{
	TD_LIST_P me = (TD_LIST_P)malloc(sizeof(TD_LIST_T));

	if(me!=NULL)
	{
		td_list_init(me,
					 td_list_add,
					 td_node_remove,
					 td_node_insert,
					 td_list_concatenate,
					 td_list_extract,
					 td_list_free,
					 td_list_count
					 );
	}


#ifdef MEMORY_LEAK_CHECK
	list_count++;
#endif

	return (me);
}

/*-----------------------------------------------------------------------
  FUNCTION: td_list_delete
-------------------------------------------------------------------------
/// <summary>
This function delete a td_list object.
If the list is not empty, a list free operation will be excuted and 
it will frees up memory used for nodes.
Finally it frees up memory used for the list object.
/// </summary>

[PARAMETERS]
[RETURN]
*
*/
PUBLIC	TD_STATUS td_list_delete(TD_LIST_P const me)
{


#ifdef MEMORY_LEAK_CHECK
	if(list_count == 0)
	{
		printf("WARNING!!!!!!!!!!!!!!!!function:%s,    line:%d\r\n",__FUNCTION__,__LINE__);
	}
	else
	{
		list_count--;
	}
#endif



	if(me!=NULL)
	{
		/* NORMALIZED FORMAT OF A DELETE OPRETAION 
		call td_list_cleanup routine to do some cleanups.
		then free the object. */
		td_list_cleanup(me);
		free(me);
		return (TD_OK);
	}
	else
	{
		return (TD_ERR_LIST_IS_NULL);
	}
}

/*=======================================================================
* PRIVATE FUNCTIONS
**=======================================================================
*/

/*-----------------------------------------------------------------------
  FUNCTION: td_list_init
-------------------------------------------------------------------------
/// <summary>
call by td_list_new to construct a new td_list object
/// </summary>
[PARAMETERS]
[RETURN]
*
*/
PRIVATE void td_list_init(
						TD_LIST_P const me,
						TD_STATUS (*add_func)(TD_LIST_P const me,TD_NODE_P p_node),
						TD_STATUS (*remove_func)(TD_LIST_P const me,TD_NODE_P p_node),
						TD_STATUS (*insert_func)(TD_LIST_P const me,TD_NODE_P prev_node,TD_NODE_P node),
						TD_STATUS (*concat_func)(TD_LIST_P const me,TD_LIST_P const add_list),
						TD_STATUS (*extract_func)(TD_LIST_P const me,TD_NODE_P start_node,TD_NODE_P end_node,TD_LIST_P const dst_list),
						TD_STATUS (*free_func)(TD_LIST_P const me),
						int	(*count_func)(TD_LIST_P const me)
					    )
{
	/* initialize attributes */
	me->head=NULL;
	me->tail=NULL;
	me->n_node = 0;

	/* initialize member function pointers */
	me->add		= add_func;
	me->remove	= remove_func;
	me->insert	= insert_func;
	me->concat	= concat_func;
	me->extract = extract_func;
	me->free	= free_func;
	me->count	= count_func;
	
#ifdef ENABLE_THREAD_SAFE
	pthread_mutex_init(&(me->mutex),NULL);
#endif
}

/*
brief: This routine adds a specified node to the end of a specified list. 
*/
PRIVATE TD_STATUS td_list_add(TD_LIST_P const me,TD_NODE_P node)
{
	TD_NODE_P last_node;  //currently last node in the list;

#ifdef ENABLE_THREAD_SAFE
	pthread_mutex_lock(&(me->mutex));
#endif	

	last_node = me->tail;

	if(me->n_node==0)
	{
		me->head = node;
		me->tail = node;
		node->next=NULL;
		node->prev =NULL;
	}
	else
	{
		last_node->next = node;
		node->prev = last_node;
		node->next = NULL;
		me->tail = node;
	}
	me->n_node++;

#ifdef ENABLE_THREAD_SAFE
	pthread_mutex_unlock(&(me->mutex));
#endif		

	return (TD_OK);
}

#if 0
PUBLIC void td_list_remove_mem_node(TD_LIST_P const me)
{
	TD_NODE_P node,next;

	//printf("td_list_remove_mem_node...\r\n");

#ifdef ENABLE_THREAD_SAFE
	pthread_mutex_lock(&(me->mutex));
#endif	
	
	if(me != NULL)
	{
		node = me->head;
		while(node != NULL)
		{
			next = node->next;
			mem_free(node);
			node = next;
		}
		me->n_node = 0;
		me->head = NULL;
		me->tail = NULL;

		#ifdef ENABLE_THREAD_SAFE
		pthread_mutex_unlock(&(me->mutex));
		#endif			

		return ;
	}
	else
	{
		#ifdef ENABLE_THREAD_SAFE
		pthread_mutex_unlock(&(me->mutex));
		#endif	
		
		return ;
	}
}
#endif

/*

brief: This routine deletes a specified node from a specified list. 
取消错误检查，由用户保证要删除的node是属于list me 的。

注意:被remove的node占用的内存没有在该方法中释放，
因此user必须在调用remove方法之后释放node占用的内存，否则会造成内存泄露。
这里不进行内存释放的原因是list的extract方法需要用到remove之后的node。

*/

PRIVATE TD_STATUS td_node_remove(TD_LIST_P const me,TD_NODE_P node)
{	
#ifdef ENABLE_THREAD_SAFE
	pthread_mutex_lock(&(me->mutex));
#endif

	if(node==NULL || me->n_node==0)
	{
	#ifdef ENABLE_THREAD_SAFE
		pthread_mutex_unlock(&(me->mutex));
	#endif	
		return(TD_ERR_NODE_NOT_FOUND);
	}

	if(node == me->head)			//要删除的node是head
	{
		me->head = (me->head)->next;		//调整头的位置，此时me->head可能等于NULL
			

		if(node->next != NULL)
		{
			(node->next)->prev = NULL;	//自己从下一个节点的prev指针中摘掉
				node->next = NULL;			//把自己的后向指针指向NULL
		}
		else  //独一节点	,必须调整tail指针
		{
			me->tail = NULL;
		}
	}
	else if(node == me->tail)			//要删除的node是tail
	{
		me->tail = me->tail->prev;		//调整尾的位置

		if(node->prev!=NULL)
		{
			(node->prev)->next = NULL;	//把自己从上一个节点的next指针中摘掉
			node->prev=NULL;			//把自己的前向指针指向NULL
		}
		else //独一节点	,必须调整head指针
		{
			me->head = NULL;
		}
	}
	else
	{
		(node->prev)->next = node->next;	//调整prev节点的指针
		(node->next)->prev = node->prev;	//调整next节点的指针
			
		node->prev = NULL;
		node->next = NULL;
	}
	(me->n_node)--;

#ifdef ENABLE_THREAD_SAFE
	pthread_mutex_unlock(&(me->mutex));
#endif		

	return (TD_OK);
}

/*
brief: 	This routine inserts a specified node in a specified list. 
		The new node is placed following the list node prev_node. 
		If prev_node is NULL, the node is inserted at the head of the list. 	
*/
PRIVATE TD_STATUS td_node_insert(TD_LIST_P const me,TD_NODE_P prev_node,TD_NODE_P node)
{

#ifdef ENABLE_THREAD_SAFE
	pthread_mutex_lock(&(me->mutex));
#endif		

	if(prev_node==NULL)
	{
		//空链表
		if(me->head == NULL)
		{
			#if 0
			printf(">插入空链表...(%X)\r\n",node);
			printf("当前链表信息:\r\n");
			printf("head=%X\r\n",me->head);
			printf("tail=%X\r\n",me->tail);
			printf("-------------------\r\n");
			printf("插入节点...\r\n");
			#endif
			
			me->head=node;
			me->tail=node;

			me->head->next = NULL;
			me->head->prev = NULL;

			me->tail->next = NULL;
			me->tail->prev = NULL;

			#if 0
			printf("------head:%X\r\n",me->head);
			printf("head->prev:%X\r\n",me->head->prev);
			printf("head->next:%X\r\n",me->head->next);
			printf("------tail:%X\r\n",me->tail);
			printf("tail->prev:%X\r\n",me->tail->prev);
			printf("tail->next:%X\r\n",me->tail->next);			
			printf(">插入完毕\r\n\r\n");
			#endif
			
		}
		else
		{
			#if 0
			printf(">插入非空链表...(%X)\r\n",node);
			printf("当前链表信息:\r\n");
			printf("head=%X\r\n",me->head);
			printf("tail=%X\r\n",me->tail);
			printf("-------------------\r\n");
			printf("插入节点...\r\n");
			#endif
			
			node->prev=NULL;
			node->next = me->head;	
			me->head->prev = node;
			me->head = node;

			#if 0
			printf("node->prev=%X\r\n",node->prev);
			printf("node->next=%X\r\n",node->next);

			printf("------head:%X\r\n",me->head);
			printf("head->prev:%X\r\n",me->head->prev);
			printf("head->next:%X\r\n",me->head->next);
			printf("------tail:%X\r\n",me->tail);
			printf("tail->prev:%X\r\n",me->tail->prev);
			printf("tail->next:%X\r\n",me->tail->next);			
			
			printf(">插入完毕\r\n\r\n");
			#endif
			
		}
	}
	else
	{
		node->next = prev_node->next;
		if(prev_node->next != NULL)
		{
			(prev_node->next)->prev = node;
		}
		prev_node->next = node;
		node->prev = prev_node;
	}
	
	(me->n_node)++;

#ifdef ENABLE_THREAD_SAFE
	pthread_mutex_unlock(&(me->mutex));
#endif		
	return (TD_OK);
}

/*
brief: 	This routine concatenates the second list to the end of the first list. 
		The second list is left empty. 
		Either list (or both) can be empty at the beginning of the operation.
*/
PRIVATE  TD_STATUS  td_list_concatenate(TD_LIST_P const me,TD_LIST_P const add_list)
{
	TD_NODE_P last_node;
	int me_list_n_node;
	int add_list_n_node;

	me_list_n_node	= me->n_node;
	add_list_n_node	= add_list->n_node;

	last_node = me->tail;		/* find the end node of the first list*/
	if(me_list_n_node == 0)	/* if the first list is empty*/
	{
		me->head = add_list->head;
		me->tail = add_list->tail;
	}
	else
	{
		last_node->next = add_list->head;
		if(add_list->head != NULL)
		{
			(add_list->head)->prev = last_node;
		}
		if(add_list->tail!=NULL)
		{
			me->tail = add_list->tail;
		}
	}

	me->n_node = me_list_n_node + add_list_n_node;
	add_list->head = NULL;
	add_list->tail = NULL;
	add_list->n_node = 0;

	return (TD_OK);
}

/*
brief: 	This routine extracts the sublist that starts with start_node and 
		ends with end_node from list "me". It places the extracted list in dst_list.
NOTE :	This operation has no parameters validity check, which means the user MUST 
		make sure that the start_node and the end_node not only belong to "me" list
		but also have the right link order.
*/
PRIVATE  TD_STATUS  td_list_extract(TD_LIST_P const me,TD_NODE_P start_node,TD_NODE_P end_node,TD_LIST_P const dst_list)
{
	TD_NODE_P node,next,end;

	if(start_node == NULL || end_node == NULL || start_node == end_node)
	{
		return (TD_ERR_INVALID_PARAM) ;
	}

	end = end_node->next;
	node = start_node;
	while(node != end)
	{
		next = node->next;
		me->remove(me,node);
		dst_list->add(dst_list,node);
		node = next;
	}
	return (TD_OK);
}

/*
brief:	This routine turns any list into an empty list. 
		It also frees up memory used for nodes.  
*/
PRIVATE  TD_STATUS  td_list_free(TD_LIST_P const me)
{
	TD_NODE_P node,next;



#ifdef ENABLE_THREAD_SAFE
	pthread_mutex_lock(&(me->mutex));
#endif	
	
	if(me != NULL)
	{
		node = me->head;
		while(node != NULL)
		{
			next = node->next;
			free(node);
			node = next;
		}
		me->n_node = 0;
		me->head = NULL;
		me->tail = NULL;

		#ifdef ENABLE_THREAD_SAFE
		pthread_mutex_unlock(&(me->mutex));
		#endif			

		return (TD_OK);
	}
	else
	{
		#ifdef ENABLE_THREAD_SAFE
		pthread_mutex_unlock(&(me->mutex));
		#endif	

		#ifdef ENABLE_THREAD_SAFE
		pthread_mutex_destroy(&(me->mutex));
		#endif		
		
		return (TD_ERR_LIST_IS_NULL);
	}
}

/*
brief: This routine returns the number of nodes in a specified list. 
*/
PRIVATE int td_list_count(TD_LIST_P const me)
{
	return (me->n_node);
}

PRIVATE void td_list_cleanup(TD_LIST_P const me)
{

	me->free(me);
	
}
/* ----------------------- end of td_list class ----------------------- */


#define DDDDD

#ifdef DDDDD


/*
+-----------------------------------------------------------------------+
|----------------------+---------------------------+--------------------|
|----------------------| D E M O N S T R A T I O N |--------------------|
|----------------------+---------------------------+--------------------|
|-----------------------------------------------------------------------|
| Description:															|
| the following demo code shows how to use the td_list library.			|
+-----------------------------------------------------------------------+
*/

/* User-defined structure must reserve space for the TD_NODE_T field */
typedef struct my_node_t
{
	/* TD_LIST required field */
	TD_NODE_T node;					// 必须是实例化的node,不能是指针
	
	/* user-defined data field*/
	int  id;
	char name[12];

}MY_NODE_T,*MY_NODE_P;

MY_NODE_P init_node(int i,const char* str);
void print_node(TD_NODE_P node);
void print_list(TD_LIST_P list);
void remove_node(TD_LIST_P list,const char* p);


/*--------------------------------------------------------------------+
| the demo entry:  demo_td_list										  |
----------------------------------------------------------------------*/
void demo_td_list(void)
{
	TD_LIST_P my_list;		//declare a td_list pointer
	MY_NODE_P my_node;		//declare a my_node pointer

	TD_LIST_P add_list;		//declare a td_list pointer
	TD_LIST_P dst_list;		//declare a td_list pointer

	printf("\n>> The usage of the [new] operation.\n");
	my_list = td_list_new();	//instantiation of the my_list object
	if(my_list == NULL)
	{
		return;
	}
	print_list(my_list);

	printf("\n>> The usage of the [add] operation.\n");
	my_node = init_node(1,"hehao");
	my_list->add(my_list,(TD_NODE_P)my_node);
	my_node = init_node( 2,"laowang");
	my_list->add(my_list,(TD_NODE_P)my_node);
	my_node = init_node( 3,"laoliu");	
	my_list->add(my_list,(TD_NODE_P)my_node);
	my_node = init_node( 4,"laowu");
	my_list->add(my_list,(TD_NODE_P)my_node);
	print_list(my_list);

	printf("\n>> The usage of the [remove] operation.\n");
	remove_node(my_list,"laowang");				  //remove "laowang"
	my_node = (MY_NODE_P)(my_list->head->next);
	my_list->remove(my_list,(TD_NODE_P)my_node); //remove head->next
	free(my_node);
	print_list(my_list);

	printf("\n>> The usage of the [insert] operation.\n");
	my_node = init_node(5,"new_guy_1");
	my_list->insert(my_list,my_list->head,(TD_NODE_P)my_node);
	my_node = init_node(6,"new_guy_2");
	my_list->insert(my_list,my_list->head->next,(TD_NODE_P)my_node);
	print_list(my_list);


	printf("\nPRESS ANY KEY TO CONTINUE...PRESS ANY KEY TO CONTINUE...");
	getchar();
	printf("\n>> The usage of the [concat] operation.\n");
	add_list = td_list_new();		//instantiation of the my_list object
	if(add_list == NULL)
	{
	return;
	}
	my_node = init_node(10,"jack");
	add_list->add(add_list,(TD_NODE_P)my_node);
	my_node = init_node( 20,"john");
	add_list->add(add_list,(TD_NODE_P)my_node);
	my_node = init_node( 30,"lucy");	
	add_list->add(add_list,(TD_NODE_P)my_node);
	my_node = init_node( 40,"luke");
	add_list->add(add_list,(TD_NODE_P)my_node);

	my_list->concat(my_list,add_list);
	printf("+++++++++++++ my_list +++++++++++++\n");
	print_list(my_list);
	printf("+++++++++++++ add_list +++++++++++++\n");
	print_list(add_list);

	printf("\nPRESS ANY KEY TO CONTINUE...PRESS ANY KEY TO CONTINUE...");
	getchar();
	printf("\n>> The usage of the [extract] operation.\n");
	dst_list = td_list_new();		//instantiation of the my_list object
	if(dst_list == NULL)
	{
	return;
	}
	my_list->extract(my_list,my_list->head,my_list->tail->prev,dst_list);
	printf("+++++++++++++ my_list +++++++++++++\n");
	print_list(my_list);
	printf("+++++++++++++ dst_list +++++++++++++\n");
	print_list(dst_list);

	printf("\nPRESS ANY KEY TO CONTINUE...PRESS ANY KEY TO CONTINUE...");
	//getchar();
	printf("\n>> The usage of the [free] operation.\n");
	my_list->free(my_list);
	print_list(my_list);

	printf("\n>> The usage of the [delete] operation.\n");
	td_list_delete(my_list);
	td_list_delete(add_list);
	td_list_delete(dst_list);
	printf("\n>> End of the td_list demonstration.\n");
	

}

/* ----------------------------------------------------------------- */
/* ------------------------- SUB-FUNCTIONS ------------------------- */
/* ----------------------------------------------------------------- */

MY_NODE_P init_node(int i,const char* str)
{
	MY_NODE_P node;
	node = (MY_NODE_P)malloc(sizeof(MY_NODE_T));
	(node)->id = i;
	strcpy((node)->name,str);

	return (node);
}

void print_node(TD_NODE_P node)
{
	if(node==NULL)
	{
		printf("err:node is NULL\n");
		return;
	}
	printf("id  = %d\n",((MY_NODE_P)node)->id);
	printf("name= %s\n",((MY_NODE_P)node)->name);
}

void print_list(TD_LIST_P list)
{
	int i=0;
	TD_NODE_P  node;
	
	if(list == NULL)
	{
		printf("err:list is NULL\n");	
		return;
	}

	node  = list->head;
	while(node!=NULL)
	{
		i++;
		printf("---Node:%02d---[%p]\n",i,node);
	    print_node(node);
		node = node->next;
	}
	printf("-------------\n");
	printf("There are %d nodes in the list.\n",list->count(list));
}

void remove_node(TD_LIST_P list,const char* p)
{
	TD_NODE_P node;
	node = list->head;
	while(node!=NULL)
	{
		if(strcmp(((MY_NODE_P)node)->name,p) == 0)
		{
			list->remove(list,node);		
			free((MY_NODE_P)node);
			break;
		}
		node = node->next;
	}
}

/* -------------------------- 测 试 用 例 -------------------------- */
TD_LIST_P list1;
MY_NODE_P node;
char name [][12]={
"老大","老二","老三","老刘","老毛",
"老谭","老外","老李","老何","老五"
};

/*
用例1:测试list的创建与添加。
*/
char list_test_case1(void)
{
	int i;
	list1 = td_list_new();
	if(list1==NULL)
	{
		return -1;
	}
	for(i=0;i<10;i++)
	{
		node = (MY_NODE_P)malloc(sizeof(MY_NODE_T));
		node->id = i+1;
		strcpy(node->name,&name[i][0]);
		list1->add(list1,(TD_NODE_P)node);
	}
	print_list(list1);
	getchar();
	return 0;
}

/*
用例2: 测试list的删除
*/
char list_test_case2(void)
{
	//删除头节点
	printf("\n删除头结点:\n");
	list1->remove(list1,list1->head);
	print_list(list1);
	getchar();
	//删除尾节点
	printf("\n删除尾节点:\n");
	list1->remove(list1,list1->tail);
	print_list(list1);
	getchar();
	//删除中间节点
	printf("\n删除中间节点:\n");
	while(list1->head->next != NULL)
		list1->remove(list1,list1->head->next);
	print_list(list1);
	getchar();
	//删除独一节点
	printf("\n删除独一节点:\n");
	list1->remove(list1,list1->tail);
	print_list(list1);
	getchar();

	return 0;
}

/*
用例3: 测试list的插入
*/
void list_test_case3(void)
{
	node = (MY_NODE_P)malloc(sizeof(MY_NODE_P));
	node->id = 100;
	strcpy(node->name,"new guy1");
	//在中间节点插入
	list1->insert(list1,list1->head->next,(TD_NODE_P)node);

	node = (MY_NODE_P)malloc(sizeof(MY_NODE_P));
	node->id = 101;
	strcpy(node->name,"new guy2");
	//在头节点前插入
	list1->insert(list1,list1->head->prev,(TD_NODE_P)node);

	node = (MY_NODE_P)malloc(sizeof(MY_NODE_P));
	node->id = 103;
	strcpy(node->name,"new guy3");
	//在尾节点前插入
	list1->insert(list1,list1->tail,(TD_NODE_P)node);

	print_list(list1);

	td_list_delete(list1);
	getchar();
}
#endif
