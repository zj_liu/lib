#################################################################
#					Toolchain configuration						#
#################################################################
RM = rm -f
SHELL = /bin/bash

CC = gcc-7

#################################################################
#					Project configuration						#
#################################################################

ROOT =/home/gmeat/lib

#################################################################
#					Gateaway SDK Sysroot						#
#################################################################
MY_SYSROOTS = /opt/fsl-imx-fb/4.14-sumo/sysroots/cortexa7hf-neon-poky-linux-gnueabi


#################################################################
#					Gateaway SDK Libreary						#
#################################################################
INCLUDE = -I./include/ 

#################################################################
#					Gateaway APP Sources Path					#
#################################################################
SRC_PATH = ./src/

#################################################################
#					Gateaway APP Build Folder					#
#################################################################
BUILD_DIR_ROOT = ./OBJ
BUILD_DIR_BIN = ./bin
LIB=./lib
PROJ_NAME=main
# C source files
SRC_DIRS = $(shell find $(SRC_PATH) -maxdepth 6 -type d)
C_SRCS = $(foreach dir, $(SRC_DIRS), $(wildcard $(dir)/*.c))

#################################################################
#						Do formatting							#
#################################################################
#LIBS = -Wl,--start-group -llogger_c -ldl -lm -Wl,--end-group  -lm -lpthread
# LIBS =  -Wl,--start-group -lreadline -lglib-2.0 -ldbus-1 -lbluetooth -ljson-c -lsqlite3 -lmosquitto -lcurl -lssl -lcrypto -lpthread -ldl -lgcc -lc -lrt -Wl,--end-group

# flags for C compiler  -Wall -Werror
#CFLAGS+= $(INCLUDE) -DPRODUCT_COAL $(addprefix -I,${SRC_PATH}) -march=armv7-a -mfpu=neon -mfloat-abi=hard -mcpu=cortex-a9 -fPIC -g  -Wall -Wl,-rpath=/usr/lib/ 
CFLAGS+= $(INCLUDE) 
LDFLAGS= $(LIB)

OBJS = $(patsubst %.c,%.o,$(C_SRCS))

# ROOT=$(shell pwd)

.PHONY: clean

all:  $(BUILD_DIR_BIN)/$(PROJ_NAME) 
#####################################
#			.o -> .elf				#
#####################################
$(BUILD_DIR_BIN)/$(PROJ_NAME): $(OBJS)
	$(CC)  $(addprefix $(BUILD_DIR_ROOT)/, $(notdir $^)) -o $@ 
#####################################
#			.c -> .o				#
#####################################
%.o : %.c
	$(CC) $(CFLAGS) -g -c -o $(addprefix $(BUILD_DIR_ROOT)/, $(notdir $@)) $^
	@echo -e "\033[44;37m Compile done \033[0m"

clean:
	@echo -e "\033[41;37mRemoving\033[0m *.o"
	@$(RM) -rf $(BUILD_DIR_ROOT)
	@echo -e "\033[42;37mOK\033[0m"
